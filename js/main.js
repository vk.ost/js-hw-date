"use strict"

// 1) Экранирование это возможность использовать специальные символы как обычные, такие как 
//    косая черта.
// 2) Function Declaration; Function Expressionl; Arrow Function; 
// 3) Hoisting это возможность взаимодействовать с переменными и функциями до их обьявления.

function createNewUser(){
    let usersFirstName = prompt("Enter your name:");
    let usersLastName = prompt("Enter your last name:");
    let usersBirthday = prompt("Enrer your birthdaydate 'dd.mm.yyyy'");
    let usersBirthdayToForm = new Date(+usersBirthday.slice(6), usersBirthday.slice(3,5)-1,
     +usersBirthday.slice(0,2));
    const newUser = {
        firstName: usersFirstName,  
        lastName: usersLastName,
        birthday: usersBirthdayToForm,
        getLogin(){
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        setFirstName(val){
            Object.defineProperty(newUser, 'firstName', {
                writable: true
             });
             this.firstName = val
             Object.defineProperty(newUser, 'firstName', {
                writable: false
             });
        },
        setLastName(val){
            Object.defineProperty(newUser, 'lastName', {
                writable: true
             });
            this.lastName = val
            Object.defineProperty(newUser, 'lastName', {
                writable: false
             });
        },
        getAge(){
            let now = new Date();
            let age = (now.getTime()-this.birthday.getTime())/(1000*3600*24*365);
            return parseInt(age);
        },
        getPasswort(){
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${usersBirthdayToForm.getFullYear()}`;
        }
    }
    Object.defineProperty(newUser, 'firstName', {
       writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        writable: false
     });
    return newUser;
}
const newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPasswort());
// в методе getAge я получаю возраст пользователя по формуле, но она не берет во внимание 
// высокостные года и идет сдвиг на 1 день каждые четыре года, нужно ли это вообще 
// учитывать?
